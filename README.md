Localization Bundles for the projectdoc Toolbox
===============================================


##Overview

This is a free add-on for [projectdoc](https://www.smartics.eu/confluence/display/PDAC1/) for Confluence.

It currently provides no localization bundles.

##Fork me!
Feel free to fork this project to provide localized resources for your installation.

The source code is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
